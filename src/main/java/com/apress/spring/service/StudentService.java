package com.apress.spring.service;

import com.apress.spring.model.Student;

public interface StudentService {
    Student save(Student student);
}