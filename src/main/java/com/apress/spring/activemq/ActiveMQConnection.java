package com.apress.spring.activemq;

import com.apress.spring.model.Student;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class ActiveMQConnection {
    public void getConnection(Student student) {
        try {
            ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination
            Destination destination = session.createQueue("testMQ");

            // Create a MessageProducerActive from the Session to the Queue
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // Create a messages
            TextMessage message = session.createTextMessage(student.getLastname() + " " + student.getCompetence());

            //sending message
            producer.send(message);
            session.close();
            connection.close();
            System.out.println("Message sent");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}