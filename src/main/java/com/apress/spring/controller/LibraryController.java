package com.apress.spring.controller;

import com.apress.spring.activemq.ActiveMQConnection;
import com.apress.spring.model.Student;
import com.apress.spring.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

@Controller
public class LibraryController {
    @Autowired
    StudentService studentService;

    @GetMapping("/library")
    public String showPage(Model model) {
        model.addAttribute("title", "Библиотека");
        model.addAttribute("someTestMessage", new Student()); //assume SomeBean has a property called datePlanted
        return "mainPage";
    }

    @RequestMapping(value = "/library", method = RequestMethod.POST)
    public String showPage(@ModelAttribute("someTestMessage") Student student,
                           Model model, Student studentBody) {
        model.addAttribute("title", "Библиотека");
        ActiveMQConnection activeMQConnection = new ActiveMQConnection();
        activeMQConnection.getConnection(student);
        studentService.save(studentBody);
        return "mainPage";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView user(ModelAndView modelAndView) {
        modelAndView.setViewName("login");
        return modelAndView;
    }
}