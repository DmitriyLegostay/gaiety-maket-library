-- Create table
DROP TABLE IF EXISTS student CASCADE;
CREATE TABLE student (
  student_id    SERIAL PRIMARY KEY NOT NULL,
  last_name  VARCHAR(50),
  competence VARCHAR(50)
);

